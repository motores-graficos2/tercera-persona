using UnityEngine;
public class ControlJugador : MonoBehaviour 

{ 
    private Rigidbody rb; 
    public int rapidez; 
    public TMPro.TMP_Text textoCantidadRecolectados; 
    public TMPro.TMP_Text textoGanaste;
    public TMPro.TMP_Text textoHuevos;
    private int cont; 
    
    void Start() 
    { 
        rb = GetComponent<Rigidbody>();
        cont = 0;
        textoHuevos.text = "Mr.Grizz: The egg basket is ready and waiting. Don't let me down.";
        textoCantidadRecolectados.text = " ";
        textoGanaste.text = " ";
    } 
    
    private void setearTextos() 
    { 
        textoCantidadRecolectados.text = "Eggs Colected: " + cont.ToString();
        if (cont >= 10) 
        { 
            textoGanaste.text = "Mr.Grizz: I gotta say - you might be the best employee I've ever had. A true profreshional.";
            textoHuevos.text = " ";
        } 
    } 
    
    private void FixedUpdate() 
    { 
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical"); 
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical); 
        rb.AddForce(vectorMovimiento * rapidez); 
    } 
    
    private void OnTriggerEnter(Collider other) 
    { 
        if (other.gameObject.CompareTag("coleccionable") == true) 
        {
            textoHuevos.text = " ";
            cont = cont + 1; 
            setearTextos(); 
            other.gameObject.SetActive(false); 
        }
    } 
}
